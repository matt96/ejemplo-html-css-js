// ----------- Datos ----------
const divisas_data = {
    "AED": "United Arab Emirates Dirham",
    "AFN": "Afghan Afghani",
    "ALL": "Albanian Lek",
    "AMD": "Armenian Dram",
    "ANG": "Netherlands Antillean Guilder",
    "AOA": "Angolan Kwanza",
    "ARS": "Argentine Peso",
    "AUD": "Australian Dollar",
    "AWG": "Aruban Florin", 
    "AZN": "Azerbaijani Manat",
    "BAM": "Bosnia-Herzegovina Convertible Mark",
    "BBD": "Barbadian Dollar",
    "BDT": "Bangladeshi Taka",
    "BGN": "Bulgarian Lev",
    "BHD": "Bahraini Dinar",
    "BIF": "Burundian Franc",
    "BMD": "Bermudan Dollar",
    "BND": "Brunei Dollar",
    "BOB": "Bolivian Boliviano",
    "BRL": "Brazilian Real",
    "BSD": "Bahamian Dollar",
    "BTC": "Bitcoin",
    "BTN": "Bhutanese Ngultrum",
    "BWP": "Botswanan Pula",
    "BYR": "Belarusian Ruble",
    "BZD": "Belize Dollar",
    "CAD": "Canadian Dollar",
    "CDF": "Congolese Franc",
    "CHF": "Swiss Franc",
    "CLF": "Chilean Unit of Account (UF)",
    "CLP": "Chilean Peso",
    "CNY": "Chinese Yuan",
    "COP": "Colombian Peso",
    "CRC": "Costa Rican Colón",
    "CUC": "Cuban Convertible Peso",
    "CUP": "Cuban Peso",
    "CVE": "Cape Verdean Escudo",
    "CZK": "Czech Republic Koruna",
    "DJF": "Djiboutian Franc",
    "DKK": "Danish Krone",
    "DOP": "Dominican Peso",
    "DZD": "Algerian Dinar",
    "EEK": "Estonian Kroon",
    "EGP": "Egyptian Pound",
    "ERN": "Eritrean Nakfa",
    "ETB": "Ethiopian Birr",
    "EUR": "Euro",
    "FJD": "Fijian Dollar",
    "FKP": "Falkland Islands Pound",
    "GBP": "British Pound Sterling",
    "GEL": "Georgian Lari",
    "GGP": "Guernsey Pound",
    "GHS": "Ghanaian Cedi",
    "GIP": "Gibraltar Pound",
    "GMD": "Gambian Dalasi",
    "GNF": "Guinean Franc",
    "GTQ": "Guatemalan Quetzal",
    "GYD": "Guyanaese Dollar",
    "HKD": "Hong Kong Dollar",
    "HNL": "Honduran Lempira",
    "HRK": "Croatian Kuna",
    "HTG": "Haitian Gourde",
    "HUF": "Hungarian Forint",
    "IDR": "Indonesian Rupiah",
    "ILS": "Israeli New Sheqel",
    "IMP": "Manx pound",
    "INR": "Indian Rupee",
    "IQD": "Iraqi Dinar",
    "IRR": "Iranian Rial",
    "ISK": "Icelandic Króna",
    "JEP": "Jersey Pound",
    "JMD": "Jamaican Dollar",
    "JOD": "Jordanian Dinar",
    "JPY": "Japanese Yen",
    "KES": "Kenyan Shilling",
    "KGS": "Kyrgystani Som",
    "KHR": "Cambodian Riel",
    "KMF": "Comorian Franc",
    "KPW": "North Korean Won",
    "KRW": "South Korean Won",
    "KWD": "Kuwaiti Dinar",
    "KYD": "Cayman Islands Dollar",
    "KZT": "Kazakhstani Tenge",
    "LAK": "Laotian Kip",
    "LBP": "Lebanese Pound",
    "LKR": "Sri Lankan Rupee",
    "LRD": "Liberian Dollar",
    "LSL": "Lesotho Loti",
    "LTL": "Lithuanian Litas",
    "LVL": "Latvian Lats",
    "LYD": "Libyan Dinar",
    "MAD": "Moroccan Dirham",
    "MDL": "Moldovan Leu",
    "MGA": "Malagasy Ariary",
    "MKD": "Macedonian Denar",
    "MMK": "Myanma Kyat",
    "MNT": "Mongolian Tugrik",
    "MOP": "Macanese Pataca",
    "MRO": "Mauritanian Ouguiya",
    "MTL": "Maltese Lira",
    "MUR": "Mauritian Rupee",
    "MVR": "Maldivian Rufiyaa",
    "MWK": "Malawian Kwacha",
    "MXN": "Mexican Peso",
    "MYR": "Malaysian Ringgit",
    "MZN": "Mozambican Metical",
    "NAD": "Namibian Dollar",
    "NGN": "Nigerian Naira",
    "NIO": "Nicaraguan Córdoba",
    "NOK": "Norwegian Krone",
    "NPR": "Nepalese Rupee",
    "NZD": "New Zealand Dollar",
    "OMR": "Omani Rial",
    "PAB": "Panamanian Balboa",
    "PEN": "Peruvian Nuevo Sol",
    "PGK": "Papua New Guinean Kina",
    "PHP": "Philippine Peso",
    "PKR": "Pakistani Rupee",
    "PLN": "Polish Zloty",
    "PYG": "Paraguayan Guarani",
    "QAR": "Qatari Rial",
    "RON": "Romanian Leu",
    "RSD": "Serbian Dinar",
    "RUB": "Russian Ruble",
    "RWF": "Rwandan Franc",
    "SAR": "Saudi Riyal",
    "SBD": "Solomon Islands Dollar",
    "SCR": "Seychellois Rupee",
    "SDG": "Sudanese Pound",
    "SEK": "Swedish Krona",
    "SGD": "Singapore Dollar",
    "SHP": "Saint Helena Pound",
    "SLL": "Sierra Leonean Leone",
    "SOS": "Somali Shilling",
    "SRD": "Surinamese Dollar",
    "STD": "São Tomé and Príncipe Dobra",
    "SVC": "Salvadoran Colón",
    "SYP": "Syrian Pound",
    "SZL": "Swazi Lilangeni",
    "THB": "Thai Baht",
    "TJS": "Tajikistani Somoni",
    "TMT": "Turkmenistani Manat",
    "TND": "Tunisian Dinar",
    "TOP": "Tongan Paʻanga",
    "TRY": "Turkish Lira",
    "TTD": "Trinidad and Tobago Dollar",
    "TWD": "New Taiwan Dollar",
    "TZS": "Tanzanian Shilling",
    "UAH": "Ukrainian Hryvnia",
    "UGX": "Ugandan Shilling",
    "USD": "United States Dollar",
    "UYU": "Uruguayan Peso",
    "UZS": "Uzbekistan Som",
    "VEF": "Venezuelan Bolívar Fuerte",
    "VND": "Vietnamese Dong",
    "VUV": "Vanuatu Vatu",
    "WST": "Samoan Tala",
    "XAF": "CFA Franc BEAC",
    "XAG": "Silver (troy ounce)",
    "XAU": "Gold (troy ounce)",
    "XCD": "East Caribbean Dollar",
    "XDR": "Special Drawing Rights",
    "XOF": "CFA Franc BCEAO",
    "XPF": "CFP Franc",
    "YER": "Yemeni Rial",
    "ZAR": "South African Rand",
    "ZMK": "Zambian Kwacha (pre-2013)",
    "ZMW": "Zambian Kwacha",
    "ZWL": "Zimbabwean Dollar"
};

const divisas_iniciales =["USD","CRC","ARS","AUD","BRL","CAD","COP","CUP","JPY","SVC"];

//---------------------- funcion para ocultar una sección-div ---------------------- 

function mostrar_ocultar($id_div) {
    $($id_div).animate({
      bottom: "+=600",
      height: "toggle",
      opacity: "toggle"
    }, {
      duration: 1000,
      queue: false
    });    
}

// --------------------- Cargar de pagina --------------------
function load_page(){
    
    mostrar_ocultar('#origen'); 
    cargar_panel_divisas_general()   
    seleccionar_divisas_iniciales();
    copy_origen_checkbox();
    tomar_divisas_conversion(); 
}

function cargar_panel_divisas_general(){
    for(var divisa in divisas_data){
        var descripcion = " | "+divisa+" | "+divisas_data[divisa];
        add_check_box("1",divisa, descripcion, "valores-origen", "origen-checkbox");
    }
}

function seleccionar_divisas_iniciales(){
    for (var divisa in divisas_iniciales){
        document.getElementById("1"+divisas_iniciales[divisa]).setAttribute("checked", "checked");
    }
}

// --------------------- Funciones de manejo de divisas --------------------

//copia las divisas seleccionadas a evaluar
function copy_origen_checkbox(){
    $('.origen-checkbox:checked').each(
        function() {
            if (!$("#"+"2"+$(this).val()).length > 0){
                var descripcion = " | "+$(this).val()+" | "+divisas_data[$(this).val()];
                add_check_box("2",$(this).val(), descripcion, "divisas", "divisas-checkbox");
            } 
        }
    );    
}

//elimina divisas seleccionadas a evaluar 
function delete_divisas_checkbox(){
    $('.divisas-checkbox:checked').each(
        function() {
            remove_checkbox("divisas", "label-"+"2"+$(this).val(), "2"+$(this).val());
        }
    );    
}

//remueve una divisa del panel de divisasa evalar
function remove_checkbox($container, $label_id, $checkbox_id){
    var label = document.getElementById($label_id);
    var checkbox = document.getElementById($checkbox_id);
    document.getElementById($container).removeChild(label);
    document.getElementById($container).removeChild(checkbox);
}

// cargar el panel de conversion con las divosas a evaluar
function tomar_divisas_conversion(){
    $("#conversiones").empty();
    $('.divisas-checkbox').each(
        function() {
            add_text_box($(this).val(),"conversiones","textbox_divisa");
        }
    );    
}

// --------------------- Manejo de checkbox y textbox --------------------

// selecciona la bandera del del pais basado en un css, modifca las clases 
function get_litlle_class_flag($divisa){
    
    var lower_divisa = $divisa;
    lower_divisa = lower_divisa.toLowerCase();
    var first_part = "flag-icon-background flag-icon flag-icon-";
    var new_divisa =lower_divisa.substr(0, lower_divisa.length-1);
    var second_part = " flag-icon-squared";
    var full_string_class = first_part + new_divisa +second_part;
    return full_string_class;
} 

// crea una eqtiueta con bandera
function crear_flag_label($id_value, $value, $description, $classes){
    // crea la etiqueta asociada
    var label= document.createElement("label");
    label.className += $classes;
    label.setAttribute("id", "label-"+$id_value+$value);
    // crea la descripcion de la etiqueta
    var description = document.createTextNode($description);
    // crea area de la badenra
    var span = document.createElement("span");    
    span.className += get_litlle_class_flag($value);
    //Carga la etiqueta
    label.appendChild(span);
    label.appendChild(description);
    label.setAttribute("for", $id_value+$value);
    return label;
}

function crear_checkbox($id_value, $value, $checkbox_class){
    var checkbox = document.createElement("input");
    checkbox.setAttribute("id", $id_value+$value);
    checkbox.type = "checkbox";    
    checkbox.value = $value;
    checkbox.className += $checkbox_class;
    return checkbox;
}

// agrega checkbox a un div
function add_check_box($id_value, $value, $description, $div_container, $checkbox_class){

    var label = crear_flag_label($id_value, $value, $description, "list-group-item");
    var checkbox =  crear_checkbox($id_value, $value, $checkbox_class);
    document.getElementById($div_container).appendChild(checkbox);
    document.getElementById($div_container).appendChild(label);    
}


function crear_textbox($id, $textbox_class){
    var textbox = document.createElement("input");
    textbox.type = "text";    // make the element a checkbox
    textbox.setAttribute("id", $id);
    textbox.className += "form-control "+$textbox_class;
    textbox.addEventListener('keydown', realizar_conversion);
    return textbox; 
}

// crea la seccion del textbox para la conversion
function add_text_box($id, $div_container, $textbox_class){
    // div
    var div = document.createElement("div");
    div.setAttribute("class", "form-group row");     
    // div2
    var div2 = document.createElement("div");
    div2.className += "col-sm-5";   
    // textbox
    var textbox = crear_textbox($id, $textbox_class);
    div2.appendChild(textbox);   
    // label
    var label = crear_flag_label("", $id, " | "+$id, "col-sm-3 col-form-label");     
    div.appendChild(label);
    div.appendChild(div2);   
    document.getElementById($div_container).appendChild(div);
}

// --------------------- accion keydown --------------------
function ejecutar_api($target,$source ){
    if($target!== $source){
        var $quantity = document.getElementById($source).value;     
        var conversion = get_conversion($source, $target, $quantity); 
        var text = document.getElementById($target);
        text.value = conversion;  
        console.log("valor: "+conversion+" actual: "+$source+" set: "+$target); 
    }
 
}

function realizar_conversion(){
    $('.textbox_divisa:focus').each(
        function() {
            var id_current_input = $(this).attr('id');
            $('.textbox_divisa').each(
                function() {                    
                    ejecutar_api($(this).attr('id'),id_current_input);
                }
            );
        }
    );    
}

// --------------------- manipular datos --------------------

function get_conversion($source, $target, $quantity){

    var peticion = new XMLHttpRequest();    
    var url = 'https://free.currencyconverterapi.com/api/v6/convert?q='+$source+'_'+$target+'&compact=y';
    peticion.open('GET', url, false);
    peticion.send();
    var respuesta = peticion.responseText;
    var obj = get_json_obj(respuesta);
    var cambio = get_cambio(obj);
    var conversion =  convertir_divisa(cambio, $quantity);
    return conversion;
    
}

function get_json_obj($respuesta){
    
    var obj = JSON.parse($respuesta);
    return obj;
}

function get_cambio($json){
    for(var divisa in $json){;
        for(var valor in $json[divisa]){
            return $json[divisa][valor];
        }
    }
}

function convertir_divisa($cambio, $cantidad){
    return ($cambio * $cantidad);
}